##  Run Tests

`sh ./debian/tests/run-unit-test`

##  Observation with Python3.12
Test fails when run with python3.12. The error appears to be from pandas with the following exception.

`File "/usr/lib/python3/dist-packages/pandas/_libs/__init__.py", line 13, in <module>`
    `from pandas._libs.interval import Interval`
`ModuleNotFoundError: No module named 'pandas._libs.interval'`
